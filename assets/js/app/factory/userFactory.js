'use strict';

app.factory('UserFactory', ['$http', function($http) {

  var factory = {

    getUsers: function (cb) {
      $http.get('/api/v1/mkrdiouser')
      .success(function (data, status, headers, config) {
        cb(null,data);
      })
    },

    getInvites: function (cb) {
      $http.get('/api/v1/invite')
      .success(function (data, status, headers, config) {
        cb(null,data);
      })
    },

    getAnalytics: function (cb) {
      $http.get('/api/v1/analytics')
      .success(function (data, status, headers, config) {
        cb(null,data);
      })
    },

    getQueryAnalytics: function (id, cb) {
      $http.get('/api/v1/queryplaybackdetails?queryAnalytics='+id)
      .success(function (data, status, headers, config) {
        console.log('/api/v1/queryplaybackdetails?queryAnalytics='+id, data);
        cb(null,data);
      })
    },

  };

  return factory;

}]);