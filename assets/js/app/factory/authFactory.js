'use strict';

app.factory('AuthFactory', ['$http', function($http) {

  var factory = {

    authorize: function (creds, cb) {
      var token = factory.getSessionToken();
      if(token) {
        cb(null, token);
      }
      else {
        $http.post('/auth/authenticate', creds)
        .success(function (data, status, headers, config) {
          setCookie('userToken', JSON.stringify(data), 7);
          _log('User Authorised', data);
          cb(null, token);
        })
        .error(function (data, status, headers, config) {
          console.log(data);
          cb(data.err)
        })
      }
    },

    getSessionToken: function () {
      var token = getCookie('userToken');
      if(token) {
        return JSON.parse(token);
      }
      else {
        return false;
      }
    }

  };

  return factory;

}]);

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+d.toUTCString();
  document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i=0; i<ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1);
    if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
  }
  return false;
}