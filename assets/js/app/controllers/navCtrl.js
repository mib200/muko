'use strict';

/* Nav Controller */

app.controller('navCtrl', function ($scope, $rootScope, $location, AuthFactory) {

  $scope.isAuthenticated = false;
  $scope.token = AuthFactory.getSessionToken();

  if($scope.token) {
    $scope.isAuthenticated = true;
  }

  $scope.isLinkActive = function (viewLocation) {
    return viewLocation === $location.path();
  };

  $scope.logout = function () {
    document.cookie = 'userToken=false';
    $scope.isAuthenticated = false;
    $location.path('/login');
  };

  $scope.$on('LoggedIn', function (event, data) {
    $scope.token = AuthFactory.getSessionToken();
    $scope.isAuthenticated = true;
  });

  $scope.$on('LoggedOut', function (event, data) {
    $scope.isAuthenticated = false;
  });

});
