'use strict';

/* Login Controller */

app.controller('loginCtrl', ['$scope', '$rootScope', '$location', 'AuthFactory', function ($scope, $rootScope, $location, AuthFactory) {

  var token = AuthFactory.getSessionToken();
  if(token) {
    $location.path('/admin/user');
  } else {
    $rootScope.$broadcast('LoggedOut');
  }

  $scope.login = function () {

    AuthFactory.authorize($scope.credentials, function (err) {
      if(err) {
        $scope.error = true;
        $scope.errorMsg = err;
      }
      else {
        $rootScope.$broadcast('LoggedIn');
        $location.path('/admin/user');
      }
    });
  }

}]);