'use strict';

/* User Controller */

app.controller('inviteCtrl', ['$scope', 'UserFactory', function ($scope, UserFactory) {

  $scope.sortCriteria = 'name';

  UserFactory.getInvites(function (err, invites) {
    if(!err) {
      $scope.invites = invites;
    }
  });

}]);