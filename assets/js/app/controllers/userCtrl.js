'use strict';

/* User Controller */

app.controller('userCtrl', ['$scope', 'UserFactory', function ($scope, UserFactory) {

  $scope.sortCriteria = 'firstName';

  UserFactory.getUsers(function (err, users) {
    if(!err) {
      $scope.users = users;
    }
  });

}]);