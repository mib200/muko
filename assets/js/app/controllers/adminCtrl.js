'use strict';

/* Admin Controller */

app.controller('adminCtrl', ['$scope', '$location', '$routeParams', 'AuthFactory', function ($scope, $location, $routeParams, AuthFactory) {
  var token = AuthFactory.getSessionToken();
  if(token) {
    $scope.activeTemplate = 'templates/' + $routeParams.subRoute + '.html';
  }
  else {
    $location.path('/login');
  }
}]);