'use strict';

/* Analytics Controller */

app.controller('analyticsCtrl', ['$scope', 'UserFactory', function ($scope, UserFactory) {

  $scope.showPlaylistTable = false;
  $scope.showSongsTable = false;
  $scope.showAnalyticsTable = true;

  UserFactory.getAnalytics(function (err, analytics) {
    if(!err) {
      $scope.analytics = analytics;
    }
  });

  $scope.showPlaylists = function (playlists) {
    $scope.playlists = playlists;
    $scope.showPlaylistTable = true;
    $scope.showAnalyticsTable = false;
    $scope.showSongsTable = false;
    $scope.paginationId = "playlists";
  }

  $scope.showSongsPlayed = function (analyticId) {
    $scope.showPlaylistTable = false;
    $scope.showAnalyticsTable = false;
    $scope.paginationId = "songs";
    UserFactory.getQueryAnalytics(analyticId, function (err, songs) {
      console.log(analyticId);
      if(!err) {
        $scope.songs = songs;
        $scope.showSongsTable = true;
      }
    });
  }

  $scope.showAnalyticsTableFunc = function () {
    $scope.showPlaylistTable = false;
    $scope.showSongsTable = false;
    $scope.showAnalyticsTable = true;
    $scope.paginationId = "analytics";
  }

}]);