
'use strict';

/* App Module */

var app = angular.module('app', [
  'ngRoute',
  'angularUtils.directives.dirPagination'
]);

app.config(function(paginationTemplateProvider) {
  paginationTemplateProvider.setPath('/bower_components/angular-utils-pagination/dirPagination.tpl.html');
});

app.config(['$routeProvider',

  function($routeProvider) {
    $routeProvider
    .when('/admin/:subRoute', {
      templateUrl : 'templates/admin.html',
      controller : 'adminCtrl'
    })
    .when('/login', {
      templateUrl : 'templates/login.html',
      controller : 'loginCtrl'
    })
    .otherwise('/login');
  }

]);

app.controller('appCtrl', function ($scope) {
  $scope.templates = {
    'nav': 'templates/nav.html'
  };
});

var _debug = false;
function _log () {
  if (_debug && typeof console !== 'undefined') {
    console.log.apply(console, arguments);
  }
}