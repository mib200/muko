/**
 * ArtistController
 *
 * @description :: Server-side logic for managing Artists
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  FindByName: function (req, res) {
    var packet = req.params.all();
    Artist.FindByName(packet, function (err, artists) {
      if(err) {
        sails.log.debug(err);
        return res.json({
          status: 404,
          message: err
        });  
      }
      else {
        return res.json({
          status: 200,
          artists: artists
        });
      }
    });
  }
	
};