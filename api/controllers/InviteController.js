/**
 * InviteController
 *
 * @description :: Server-side logic for managing invites
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

	create: function (req, res){
		var packet = req.params.all();
		console.log("packet", packet);
		Invite.saveInvite(packet, function (err, invite){
			if(err){
				sails.log.error("[InviteController.create] error saving invite", err);
				return res.json(500, {
					"error": err
				});
			}
			else{
				sails.log.info("[InviteController.create] saved invite");
				Mandrill.sendInviteReceivedMail(invite.email, invite.name)
				return res.json(200, invite);
			}
		});
	}

};

