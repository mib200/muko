/**
 * QueryPlaybackDetailsController
 *
 * @description :: Server-side logic for managing Queryplaybackdetails
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

 module.exports = {
 	create: function (req, res) {
 		var packet = req.params.all();
 		sails.log.debug(packet);
 		QueryPlaybackDetails.create({
 			queryAnalytics: packet.queryAnalytics,
 			songID: packet.songID,
 			songName: packet.songName,
 			buffering_time: (packet.buffering_time/1000)
 		})
 		.then(function (analytics) {
 			return res.json({
 				status: 200
 			});
 		})
 		.catch(function (err) {
 			sails.log.error(err);
 			return res.json({
 				status: 404,
 				message: err
 			});
 		})
 	},

 	updateSongDuration: function(req,res){
 		var packet = req.params.all();
 		sails.log.debug(packet);
 		QueryPlaybackDetails.update({
			queryAnalytics: packet.queryAnalytics,
			songID: packet.songID
		},
		{
			playback_time: (packet.playback_time/1000)
 		})
 		.then(function (analytics) {
 			return res.json({
 				status: 200
 			});
 		})
 		.catch(function (err) {
 			sails.log.error(err);
 			return res.json({
 				status: 404,
 				message: err
 			});
 		})	
 	}
 };

