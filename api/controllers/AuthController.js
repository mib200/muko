/**
 * AuthController
 *
 * @description :: Server-side logic for managing Auths
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  authenticate: function (req, res) {
    var email = req.param('email'),
    password = req.param('password');

    if (!email || !password) {
      return res.json(401, {err: 'email and password required'});
    }

    Admin.findOne({email: email, password: password}, function (err, user) {
      if(err) {
        return res.json(401, {
          err: 'Error Logging In !'
        });
      }
      else if(!user) {
        return res.json(401, {
          err: 'Incorrect username or password.'
        });
      }
      else {
        return res.json(200, {
          user: {email: email},
          token: TokenService.issueToken(user.id)
        });
      }
    })

  }

};

