/**
 * AnalyticsController
 *
 * @description :: Server-side logic for managing analytics
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  create: function (req, res) {
    var packet = req.params.all();
    sails.log.debug(packet);
    Analytics.create({
      key: packet.key,
      query: packet.query,
      email: packet.email,
      name: packet.name,
      queried_at: packet.timeStamp,
      playlist: JSON.parse(packet.playlist),
      muko_time_taken: (packet.muko_time_taken/1000)
    })
    .then(function (analytics) {
      return res.json({
        status: 200,
        message: analytics
      });
    })
    .catch(function (err) {
      sails.log.error(err);
      return res.json({
        status: 404,
        message: err
      });
    })
  }

};

