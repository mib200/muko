/**
 * MKRdioUserController
 *
 * @description :: Server-side logic for managing Mkrdiousers
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  create: function (req, res) {
    var packet = req.params.all();
    if (packet.userKey === "defaultuser") {
      packet.userKey = packet.userKey + "-" + packet.deviceID;
    }

    MKRdioUser.create({
      userKey: packet.userKey,
      firstName: packet.firstName,
      lastName: packet.lastName,
      deviceID: packet.deviceID
    })
    .then(function (user) {
      return res.json({
        status: 200
      });
    })
    .catch(function (err) {
      sails.log.error(err);
      return res.json({
        status: 404,
        message: err
      });
    })
  },

  updateSongThumbs: function (req, res) {
    var packet = req.params.all();
    sails.log.debug(packet);
    MKRdioUser.AddToSongsThumb(packet, function (err, user) {
      if(err) {
        sails.log.error(err);
        return res.json({
          status: 404,
          message: err
        });
      }
      else {
        return res.json({
          status: 200
        });
      }
    })
  },

  updateArtists: function (req, res) {
    var packet = req.params.all();
    MKRdioUser.UpdateArtists(packet, function (err, user) {
      if(err) {
        sails.log.error(err);
        return res.json({
          status: 404,
          message: err
        });
      }
      else {
        return res.json({
          status: 200
        });
      }
    })
  },

  updateGenres: function (req, res) {
    var packet = req.params.all();
    MKRdioUser.UpdateGenres(packet, function (err, user) {
      if(err) {
        sails.log.error(err);
        return res.json({
          status: 404,
          message: err
        });
      }
      else {
        return res.json({
          status: 200
        });
      }
    })
  },

  getCheckedGenres: function(req, res) {
    var packet = req.params.all();
    MKRdioUser.GetCheckedGenres(packet, function (err, results) {
      if(err) {
        sails.log.error(err);
        return res.json({
          status: 404,
          message: err
        });
      }
      else {
        return res.json({
          status: 200,
          message: results
        });
      }
    })
  }

	
};

