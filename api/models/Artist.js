/**
* Artist.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

function FindByName (packet, callback) {
  var regStr, regex,
  limit = parseInt(packet.limit) || 300,
  page = parseInt(packet.page) || 0;
  if(packet.name && packet.name.length) {
  regStr = packet.name.replace(/[^a-z0-9\s]/gi, '.*');
  regex = new RegExp('\\b' + regStr,"i");
  }
  else {
    regex = false;
  }
  Artist.native(function (err, artistColl) {
    if(err) {
      callback(err);
    }
    else {
      artistColl
      .find(regex ? {'artistName': regex} : {}, {'artistName': 1})
      .skip(page*limit)
      .limit(limit)
      .sort('artistName ASC')
      .toArray(function (err, artists) {
        if(err) {
          callback(err);
        }
        else {
          callback(null, artists);
        }
      })
    }
  });
}

module.exports = {

  attributes: {
  	
  	artistName: {
  		type: "string",
  		required: true
  	}

  },

  FindByName: FindByName
};

