/**
* Analytics.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  //schema: true,

  attributes: {

    key: {
      type: 'string',
      required: true
    },

    query: {
      type: 'string',
      required: true
    },

    name: {
      type: 'string',
      required: true
    },

    queried_at: {
      type: 'string'
    },

    playlist: {
      type: 'array'
    },

    muko_time_taken: {
      type: 'float'
    },

    query_playback_detail: {
      collection: 'queryplaybackdetails',
      via: "queryAnalytics"
    }

  }
};

