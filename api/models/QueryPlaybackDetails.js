/**
* QueryPlaybackDetails.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
  	songID: {
  		type: "string"
  	},

  	songName: {
  		type: "string"
  	},

  	buffering_time: {
  		type: "float"
  	},

  	playback_time: {
  		type: "integer"
  	},

  	queryAnalytics: {
  		model: 'analytics'
  	}
  }
};

