/**
* MKRdioUser.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

function AddToSongsThumb (packet, callback) {
  for(vote in packet.newVotes){
    if (vote.indexOf('.') > -1) {
      delete packet.newVotes[vote];
    };
  }
  MKRdioUser
  .update({userKey: packet.userKey}, {songsThumbInfo: packet.newVotes})
  .then(function (user) {
    callback(null, user);
  })
  .catch(function (err) {
    callback(err);
  });
}

function UpdateArtists (packet, callback) {
  var art = packet.artists.replace(/[\[\]']+/g,'').split(", ").filter(Boolean);

  MKRdioUser
  .update({userKey: packet.userKey}, {artists_selected: art})
  .then(function (user) {
    callback(null, user);
  })
  .catch(function (err) {
    callback(err);
  });
}

function UpdateGenres (packet, callback) {
  
  var art = packet.genres.replace(/[\[\]']+/g,'').split(", ").filter(Boolean);
  
  MKRdioUser
  .update({userKey: packet.userKey}, {genres_selected: art})
  .then(function (user) {
    callback(null, user);
  })
  .catch(function (err) {
    callback(err);
  });
}

function GetCheckedGenres(packet, callback) {

  MKRdioUser
  .findOne({userKey: packet.userKey})
  .then(function(user){
    Genre.find().sort('genreOrder ASC').then(function(genres){
      var userGenres = user.genres_selected;
      var finalResponse = {};
      async.each(genres, function(genre,cb){
        if(userGenres.indexOf(genre["genreTitle"]) > -1){
          finalResponse[genre["genreTitle"]] = true;
        }
        else {
          finalResponse[genre["genreTitle"]] = false;
        }
        cb();
      });
      callback(null, finalResponse);
    })
    .catch(function(err){
      callback(err);
    })
  })
  .catch(function(err){

  })
}

module.exports = {
  attributes: {
  	
  	userKey: {
  		type: "string",
  		unique: true,
      required: true
  	},

  	firstName: {
  		type: "string"
  	},

  	lastName: {
  		type: "string"	
  	},

  	genres_selected: {
  		type: "array"
  	},

  	artists_selected: {
  		type: "array"
  	},

  	songsThumbInfo: {
  		type: "json"
  	},

    deviceID: {
      type: "string"
    }
  },

  UpdateArtists: UpdateArtists,

  UpdateGenres: UpdateGenres,

  AddToSongsThumb: AddToSongsThumb,

  GetCheckedGenres: GetCheckedGenres
};