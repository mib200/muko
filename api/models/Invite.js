/**
* Invite.js
*/


function saveInvite(attributes, callback){
	Invite.create(attributes).exec(callback);
}

module.exports = {

	schema: true,

	attributes: {
  	email: {
  		type: "email",
  		unique: true,
  		required: true
  	},
  	name: {
  		type: "string",
  		required: true
  	},
  	affiliation: {
  		type: "string"  	
  	},
  	device: {
  		type: "string",
  		in : ['android', 'ios'],
  		required: true
  	},
  	ip: {
  		type: "string"
  	}
  },

  // functions
	saveInvite: saveInvite

};

