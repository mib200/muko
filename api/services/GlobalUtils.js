
module.exports = {

  errorCB: function (err) {
    sails.log.error(err);
    return res.json({
      status: 404,
      message: err
    });
  }

}