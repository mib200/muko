

var mandrill = require('mandrill-api/mandrill');
var mandrill_client = new mandrill.Mandrill('viY-mwaOZBvZW7hIg9VbYg');

// Base Message
var message = {
  "html": "",
  "text": "",
  "subject": "",
  "from_email": "admin@themuko.com",
  "from_name": "Muko Admin",
  "to": [],
  "headers": { "Reply-To": "admin@themuko.com" },
  "important": false,
  "track_opens": true,
  "track_clicks": null,
  "auto_text": null,
  "auto_html": null,
  "inline_css": null,
  "url_strip_qs": null,
  "preserve_recipients": null,
  "view_content_link": null,
  "tracking_domain": null,
  "signing_domain": null,
  "return_path_domain": null,
  "merge": true,
  "global_merge_vars": [],
  "merge_vars": [],
  "tags": [ "Welcome Email" ],
  "metadata": { "website": "www.themuko.com" },
  "recipient_metadata": [],
  "attachments": [],
  "images": []
};

module.exports = {

  sendWelcomeEmail: function (user) {

    this.sendMail({
      to: {
        "email": user.primary_email.email,
        "name": user.name,
        "type": "to"
      },
      subject: "Welcome to Hellos!",
      text: "",
      html:
          "<div>Ahoy! " + user.name + "</div><br />"
        + "<div>Welcome to Hellos!!!</div> <br />"
    });

  },

  sendVerificationCode: function(email, code){

    this.sendMail({
      to: {
        "email": email,
        "name": "",
        "type": "to"
      },
      subject: "Your verification code!",
      text: "",
      html:
          "<div>Ahoy!<br />"
        + "<div>Here is your verification code "+ code +"</div> <br />"
    });

  },



  sendPwdResetMail: function (user, resetLink) {

    this.sendMail({
      to: {
        "email": user.email,
        "name": user.displayName,
        "type": "to"
      },
      subject: "Reset Password for your Thirstt Account",
      text: "Hi "
        + user.username
        + "! You have requested for resetting your pasword. Please click on the link to proceed: "
        + resetLink,
      html: ""
    });

  },


  sendInviteReceivedMail: function (sendTo, recipientName) {
    var firstName = recipientName.trim().split(" ")[0];
    this.sendTemplateMail({
      to: {
        "email": sendTo,
        "name": recipientName,
        "type": "to"
      },
      "template_name": "inviteRequestReceived",
      "template_content": [
        {
          'name': 'name',
          'content': firstName
        }
      ],
      subject: "Thank you for joining Muko beta test",
    });
	},

  sendTemplateMail: function(mailOpts){
    var self = this;
    mandrill_client.templates.render({"template_name": mailOpts.template_name, "template_content": mailOpts.template_content}, function (result) {
      self.sendMail({
        to: mailOpts.to,
        subject: mailOpts.subject,
        html: result.html
      });
    }, function (e) {
      console.error('A mandrill error occurred 2: ' + e.name + ' - ' + e.message);
    });    
  },

  sendMail: function (mailOpts) {
    var messageToSend = _.cloneDeep(message);
    messageToSend.html = mailOpts.html;
    console.log(messageToSend.html);
    messageToSend.subject = mailOpts.subject;
    messageToSend.to.push(mailOpts.to);
    mandrill_client.messages.send(
      {"message": messageToSend, "async": true, "ip_pool": "Main Pool", "send_at": ""},
      function successCallback(result) {
        console.log('Email sent to ', result);
      },
      function errorCallback(e) {
        console.error('A mandrill error occurred: ' + e.name + ' - ' + e.message);
      }
    );
  }

};